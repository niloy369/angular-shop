import { Component, OnInit } from '@angular/core';
import {ProductService} from '../../product/shared/product.service';
import {Product} from '../../product/shared/model';
import {CartService} from '../../cart/shared/cart.service';

@Component({
  selector: 'app-shop-home',
  templateUrl: './shop-home.component.html',
  styleUrls: ['./shop-home.component.css']
})
export class ShopHomeComponent implements OnInit {
  products: Product[] = [];
  cart: Product[] = [];

  constructor(
    private productService: ProductService,
    private cartService: CartService
  ) {
    this.productService.products.subscribe(data=>{
      this.products = data;
    });

    this.cartService.cart.subscribe(data=>{
      this.cart = data;
    })
  }

  ngOnInit(): void {
    this.productService.getProducts();
  }

}
