import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ShopHomeComponent } from './shop-home/shop-home.component';
import {ShopRoutingModule} from './shop-routing.module';
import {MatGridListModule} from '@angular/material/grid-list';
import {ProductCardModule} from '../product/product-card/product-card.module';
import {FlexLayoutModule} from '@angular/flex-layout';
import {MiniCartModule} from '../cart/mini-cart/mini-cart.module';



@NgModule({
  declarations: [ShopHomeComponent],
  imports: [
    CommonModule,
    ShopRoutingModule,
    MatGridListModule,
    ProductCardModule,
    FlexLayoutModule,
    MiniCartModule
  ]
})
export class ShopModule { }
