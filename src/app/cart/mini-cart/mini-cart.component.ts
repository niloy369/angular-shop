import {Component, Input, OnInit} from '@angular/core';
import {Product} from '../../product/shared/model';
import {CartService} from '../shared/cart.service';

@Component({
  selector: 'app-mini-cart',
  templateUrl: './mini-cart.component.html',
  styleUrls: ['./mini-cart.component.css']
})
export class MiniCartComponent implements OnInit {

  cart: Product[] = [];
  subtotal: number = 0;
  constructor(
    private cartService: CartService
  ) {
    this.cartService.cart.subscribe(data=>{
      this.cart = data;
      this.subtotal = 0;
      for (let d of data){
        this.subtotal += d.quantity * d.price;
      }
    });
  }

  ngOnInit(): void {
  }

  removeProduct(index: number){
    this.cartService.removeFromCart(index);
  }

}
