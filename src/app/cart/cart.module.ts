import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CartHomeComponent } from './cart-home/cart-home.component';



@NgModule({
  declarations: [CartHomeComponent],
  imports: [
    CommonModule
  ]
})
export class CartModule { }
