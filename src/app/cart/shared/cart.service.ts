import { Injectable } from '@angular/core';
import {BehaviorSubject} from 'rxjs';
import {Product} from '../../product/shared/model';
import {ProductService} from '../../product/shared/product.service';

@Injectable({
  providedIn: 'root'
})
export class CartService {

  cart: BehaviorSubject<Product[]> = new BehaviorSubject([]);
  products: Product[] = [];
  cartLocal: Product[] = [];
  constructor(
    private productService: ProductService
  ) {
    this.productService.products.subscribe(data=>this.products = data);
  }

  addToCartClick(productID: string, quantity: number){
    // add to cart
    let cart_index = this.cartLocal.findIndex(x=> x._id == productID);
    let product_index = this.products.findIndex(x=> x._id == productID);
    if(cart_index == -1){
      let product = this.products[product_index];
      product.quantity = quantity;
      product.stock -= quantity;
      this.cartLocal.push(Object.assign({}, product));
    } else {
      let cart_product = this.cartLocal[cart_index];
      let product = this.products[product_index];

      product.stock -= quantity;
      cart_product.quantity += quantity;
    }
    this.cart.next(this.cartLocal);
  }

  removeFromCart(index: number){
    let product_index = this.products.findIndex(x=> x._id == this.cartLocal[index]._id);
    let product = this.products[product_index];
    product.stock += this.cartLocal[index].quantity;

    this.cartLocal.splice(index, 1);
    this.cart.next(this.cartLocal);
  }
}
