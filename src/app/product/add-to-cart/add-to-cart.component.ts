import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {Product} from '../shared/model';
import {CartService} from '../../cart/shared/cart.service';

@Component({
  selector: 'app-add-to-cart',
  templateUrl: './add-to-cart.component.html',
  styleUrls: ['./add-to-cart.component.css']
})
export class AddToCartComponent implements OnInit {
  @Input() product: Product;
  constructor(
    private cartService: CartService
  ) { }

  ngOnInit(): void {
  }

  addToCartClick(productID: string){
    this.cartService.addToCartClick(productID, 1)
  }

}
