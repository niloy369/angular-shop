import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ProductCardComponent } from './product-card.component';
import {MatCardModule} from '@angular/material/card';
import {AddToCartModule} from '../add-to-cart/add-to-cart.module';



@NgModule({
  declarations: [ProductCardComponent],
  exports: [
    ProductCardComponent
  ],
  imports: [
    CommonModule,
    MatCardModule,
    AddToCartModule
  ]
})
export class ProductCardModule { }
