import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {Product} from '../shared/model';

@Component({
  selector: 'app-product-card',
  templateUrl: './product-card.component.html',
  styleUrls: ['./product-card.component.css']
})
export class ProductCardComponent implements OnInit {
  _product: Product;
  @Input() set product(data: Product){
    if (data != undefined){
      this._product = data;
    }
  }

  get product(): Product{
    return this._product;
  }


  constructor() { }

  ngOnInit(): void {
  }
}
